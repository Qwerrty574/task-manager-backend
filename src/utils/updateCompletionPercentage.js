const pool = require('../config/db');
const logger = require('../config/logger');

async function updateCompletionPercentage(listId) {
    try {
        const totalTasksResult = await pool.query('SELECT COUNT(*) FROM tasks WHERE list_id = $1', [listId]);
        const completedTasksResult = await pool.query('SELECT COUNT(*) FROM tasks WHERE list_id = $1 AND status = $2', [listId, 'completed']);

        const totalTasks = parseInt(totalTasksResult.rows[0].count, 10);
        const completedTasks = parseInt(completedTasksResult.rows[0].count, 10);

        const percentage = totalTasks > 0 ? (completedTasks / totalTasks) * 100 : 0;

        await pool.query('UPDATE task_lists SET completion_percentage = $1 WHERE id = $2', [percentage, listId]);
    } catch (error) {
        logger.error(`Error updating completion percentage: ${error.message}`);
    }
}

module.exports = updateCompletionPercentage;
