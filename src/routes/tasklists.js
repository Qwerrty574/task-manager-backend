const express = require('express');
const pool = require('../config/db');
const authenticateToken = require('../middlewares/auth');
const updateCompletionPercentage = require('../utils/updateCompletionPercentage');
const logger = require('../config/logger');

const router = express.Router();

// Get all task lists for the authenticated user
router.get('/', authenticateToken, async (req, res) => {
    try {
        const result = await pool.query('SELECT * FROM task_lists WHERE user_id = $1', [req.user.id]);
        res.status(200).json(result.rows);
    } catch (error) {
        logger.error(`Error fetching task lists: ${error.message}`);
        res.status(500).json({error: 'Failed to fetch task lists'});
    }
});

// Create a new task list
router.post('/', authenticateToken, async (req, res) => {
    const {name} = req.body;
    if (!name) {
        return res.status(400).json({error: 'Name is required'});
    }

    try {
        const result = await pool.query('INSERT INTO task_lists (name, user_id) VALUES ($1, $2) RETURNING *', [name, req.user.id]);
        res.status(201).json(result.rows[0]);
    } catch (error) {
        logger.error(`Error creating task list: ${error.message}`);
        res.status(500).json({error: 'Failed to create task list'});
    }
});

// Update a task list by ID
router.put('/:id', authenticateToken, async (req, res) => {
    const {name} = req.body;
    const {id} = req.params;

    try {
        const result = await pool.query('UPDATE task_lists SET name = $1 WHERE id = $2 AND user_id = $3 RETURNING *', [name, id, req.user.id]);
        if (result.rows.length === 0) {
            return res.status(404).json({error: 'Task list not found'});
        }
        res.status(200).json(result.rows[0]);
    } catch (error) {
        logger.error(`Error updating task list: ${error.message}`);
        res.status(500).json({error: 'Failed to update task list'});
    }
});

// Delete a task list by ID
router.delete('/:id', authenticateToken, async (req, res) => {
    const {id} = req.params;

    try {
        const result = await pool.query('DELETE FROM task_lists WHERE id = $1 AND user_id = $2 RETURNING *', [id, req.user.id]);
        if (result.rows.length === 0) {
            return res.status(404).json({error: 'Task list not found'});
        }
        res.status(200).json({message: 'Task list deleted successfully'});
    } catch (error) {
        logger.error(`Error deleting task list: ${error.message}`);
        res.status(500).json({error: 'Failed to delete task list'});
    }
});

module.exports = router;
