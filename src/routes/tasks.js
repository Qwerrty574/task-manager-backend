const express = require('express');
const pool = require('../config/db');
const authenticateToken = require('../middlewares/auth');
const updateCompletionPercentage = require('../utils/updateCompletionPercentage');
const logger = require('../config/logger');

const router = express.Router();

// Get all tasks for a specific task list
router.get('/list/:listId', authenticateToken, async (req, res) => {
    const {listId} = req.params;

    try {
        const result = await pool.query('SELECT * FROM tasks WHERE list_id = $1 AND user_id = $2', [listId, req.user.id]);
        res.status(200).json(result.rows);
    } catch (error) {
        logger.error(`Error fetching tasks: ${error.message}`);
        res.status(500).json({error: 'Failed to fetch tasks'});
    }
});

// Create a new task
router.post('/', authenticateToken, async (req, res) => {
    const {title, description, list_id} = req.body;
    if (!title || !list_id) {
        return res.status(400).json({error: 'Title and list ID are required'});
    }

    try {
        const result = await pool.query('INSERT INTO tasks (title, description, list_id, user_id) VALUES ($1, $2, $3, $4) RETURNING *', [title, description, list_id, req.user.id]);
        await updateCompletionPercentage(list_id);
        res.status(201).json(result.rows[0]);
    } catch (error) {
        logger.error(`Error creating task: ${error.message}`);
        res.status(500).json({error: 'Failed to create task'});
    }
});

// Update a task by ID
router.put('/:id', authenticateToken, async (req, res) => {
    const {title, description, status} = req.body;
    const {id} = req.params;

    try {
        const result = await pool.query('UPDATE tasks SET title = $1, description = $2, status = $3 WHERE id = $4 AND user_id = $5 RETURNING *', [title, description, status, id, req.user.id]);
        if (result.rows.length === 0) {
            return res.status(404).json({error: 'Task not found'});
        }
        await updateCompletionPercentage(result.rows[0].list_id);
        res.status(200).json(result.rows[0]);
    } catch (error) {
        logger.error(`Error updating task: ${error.message}`);
        res.status(500).json({error: 'Failed to update task'});
    }
});

// Delete a task by ID
router.delete('/:id', authenticateToken, async (req, res) => {
    const {id} = req.params;

    try {
        const result = await pool.query('DELETE FROM tasks WHERE id = $1 AND user_id = $2 RETURNING *', [id, req.user.id]);
        if (result.rows.length === 0) {
            return res.status(404).json({error: 'Task not found'});
        }
        await updateCompletionPercentage(result.rows[0].list_id);
        res.status(200).json({message: 'Task deleted successfully'});
    } catch (error) {
        logger.error(`Error deleting task: ${error.message}`);
        res.status(500).json({error: 'Failed to delete task'});
    }
});

module.exports = router;
