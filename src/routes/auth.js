const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const pool = require('../config/db');
const logger = require('../config/logger');
const authenticateToken = require('../middlewares/auth');

const router = express.Router();
const tokenKey = process.env.TOKEN_KEY;

router.post('/signup', async (req, res) => {
    const {username, password, email, secretResponse} = req.body;

    if (!username || !password || !email || !secretResponse) {
        logger.warn('Signup attempt with missing fields');
        return res.status(400).json({error: 'All fields are required'});
    }

    try {
        const hashedPassword = await bcrypt.hash(password, 10);

        const result = await pool.query(
            'INSERT INTO users (username, password, email, secret_response) VALUES ($1, $2, $3, $4) RETURNING *',
            [username, hashedPassword, email, secretResponse]
        );

        const user = result.rows[0];
        const token = jwt.sign({id: user.id}, tokenKey);
        await pool.query('UPDATE users SET token=$1 WHERE id=$2', [token, user.id]);

        logger.info(`User ${user.username} signed up successfully`);
        res.status(201).json({id: user.id, token});
    } catch (error) {
        logger.error(`Signup error: ${error.message}`);
        res.status(400).json({error: error.message});
    }
});

router.post('/signin', async (req, res) => {
    const {username, password} = req.body;

    if (!username || !password) {
        logger.warn('Signin attempt with missing fields');
        return res.status(400).json({error: 'All fields are required'});
    }

    try {
        const result = await pool.query('SELECT * FROM users WHERE username=$1', [username]);
        const user = result.rows[0];

        if (user && await bcrypt.compare(password, user.password)) {
            const token = jwt.sign({id: user.id}, tokenKey);
            logger.info(`User ${username} signed in successfully`);
            res.status(200).json({id: user.id, token});
        } else {
            logger.warn(`Signin attempt failed for user ${username}`);
            res.status(401).json({error: 'Invalid credentials'});
        }
    } catch (error) {
        logger.error(`Signin error: ${error.message}`);
        res.status(400).json({error: error.message});
    }
});

router.get('/check', authenticateToken, (req, res) => {
    res.status(200).json({id: req.user.id, token: 'Authentication token is valid.'});
});

router.get('/user', authenticateToken, async (req, res) => {
    try {
        const result = await pool.query('SELECT * FROM users WHERE id=$1', [req.user.id]);
        const user = result.rows[0];

        if (!user) {
            logger.warn('User data request for non-existing user');
            return res.status(404).json({id: -1, token: 'User not found.'});
        }

        logger.info(`User data requested for user ${user.username}`);
        res.status(200).json(user);
    } catch (error) {
        logger.error(`User data request error: ${error.message}`);
        res.status(400).json({error: error.message});
    }
});

router.post('/reset', async (req, res) => {
    const {username, password, email, secretResponse} = req.body;

    if (!username || !password || !email || !secretResponse) {
        logger.warn('Password reset attempt with missing fields');
        return res.status(400).json({error: 'All fields are required'});
    }

    try {
        const result = await pool.query('SELECT * FROM users WHERE username=$1 AND email=$2 AND secret_response=$3', [username, email, secretResponse]);
        const user = result.rows[0];

        if (!user) {
            logger.warn(`Password reset attempt for non-existing user ${username}`);
            return res.status(404).json({error: 'User not found.'});
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        await pool.query('UPDATE users SET password=$1 WHERE id=$2', [hashedPassword, user.id]);

        logger.info(`Password reset successfully for user ${username}`);
        res.status(200).json({id: user.id, token: 'The user password has changed successfully.'});
    } catch (error) {
        logger.error(`Password reset error: ${error.message}`);
        res.status(400).json({error: error.message});
    }
});

router.delete('/drop', async (req, res) => {
    const {username, password} = req.body;

    if (!username || !password) {
        logger.warn('User deletion attempt with missing fields');
        return res.status(400).json({error: 'All fields are required'});
    }

    try {
        const result = await pool.query('SELECT * FROM users WHERE username=$1', [username]);
        const user = result.rows[0];

        if (!user) {
            logger.warn(`User deletion attempt for non-existing user ${username}`);
            return res.status(404).json({error: 'User not found.'});
        }

        if (!await bcrypt.compare(password, user.password)) {
            logger.warn(`User deletion attempt failed due to incorrect password for user ${username}`);
            return res.status(401).json({error: 'Invalid credentials'});
        }

        await pool.query('DELETE FROM users WHERE username=$1', [username]);
        logger.info(`User ${username} deleted successfully`);
        res.status(200).json({message: 'User deleted successfully.'});
    } catch (error) {
        logger.error(`User deletion error: ${error.message}`);
        res.status(400).json({error: error.message});
    }
});

module.exports = router;
