const jwt = require('jsonwebtoken');
const logger = require('../config/logger');

const tokenKey = process.env.TOKEN_KEY;

function authenticateToken(req, res, next) {
    const token = req.headers.authorization?.split(' ')[1];
    if (!token) {
        logger.warn('Token missing');
        return res.status(401).json({error: 'Not authorized.'});
    }

    jwt.verify(token, tokenKey, (err, user) => {
        if (err) {
            logger.warn('Invalid token');
            return res.status(403).json({error: 'Invalid token.'});
        }
        req.user = user;
        next();
    });
}

module.exports = authenticateToken;
