const express = require('express');
const cors = require('cors');
const morganMiddleware = require('./middlewares/morgan');
const errorHandler = require('./middlewares/errorHandler');
const authRoutes = require('./routes/auth');
const tasklistsRoutes = require('./routes/tasklists');
const tasksRoutes = require('./routes/tasks');

const app = express();

app.use(cors());
app.use(express.json());
app.use(morganMiddleware);

app.use('/auth', authRoutes);
app.use('/tasklists', tasklistsRoutes);
app.use('/tasks', tasksRoutes);

app.use(errorHandler);

module.exports = app;
