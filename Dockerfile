FROM mirror.gcr.io/node:lts-bookworm-slim
LABEL authors="qwerrty574"

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json .env ./

USER node

RUN npm install

COPY --chown=node:node ./src .

EXPOSE 7000

CMD [ "node", "server.js" ]